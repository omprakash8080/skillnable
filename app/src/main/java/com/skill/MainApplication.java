package com.skill;

import android.app.Application;

import com.skill.api.ApiClient;

/**
 * Created by omprakash on 29/2/16.
 */
public class MainApplication extends Application {

    @Override
    public void onCreate()
    {
        super.onCreate();
        ApiClient.init();
    }
}
