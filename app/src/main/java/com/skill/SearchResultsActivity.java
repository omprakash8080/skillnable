package com.skill;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.skill.helper.AndroidUtils;
import com.skill.helper.Constants;
import com.skill.helper.TinyDB;

public class SearchResultsActivity extends BaseActivity {

    Toolbar toolbar;
    TinyDB tinyDB;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        tinyDB = TinyDB.getInstance(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        handleIntent(getIntent());

    }


    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            if(query !=null  && query.length()>0){
                tinyDB.putString(Constants.QUERY, query);
                switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.HOMEFRAGMENT), false, R.id.fragmentContainer, false);
            }

            Log.v(" test ", "####################");
            Log.v("test", query);
//            showResults(query);

        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//       getMenuInflater().inflate(R.menu.menu_main, menu);
       return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

}
