package com.skill.api;

import android.content.Context;

import com.skill.model.*;

import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class AllApiFacade implements IAllApiFacade, IResponseSubscribe {

    private String TAG;
    ApiClient.ApiService client;
    private IResponseSubscribe responseObserver;
    //	ApplicationObjectsCollectionPool pool;

    public AllApiFacade(IResponseSubscribe responseObserver) {
        this.responseObserver = responseObserver;
        client = ApiClient.getInstance();
        //		pool = ApplicationObjectsCollectionPool.getInstance();
    }

    @Override
    public void onResponse(Object response, String tag) {
        responseObserver.onResponse(response, tag);
    }

    @Override
    public void onErrorResponse(Exception error, String tag) {
        responseObserver.onErrorResponse(error, tag);
    }

    @Override
    public void setTag(String tag) {

    }

    @Override
    public void userLogin(Context ctx, Map<String, String> keyword, final String tag) {
        client.login(keyword, new Callback<LoginResponse>() {
            @Override
            public void success(LoginResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }

        });
    }

    @Override
    public void getAllTickets(Context ctx, Map<String, String> keyword, final String tag) {
        client.getTickets(keyword, new Callback<TicktesResponse>() {
            @Override
            public void success(TicktesResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }

        });
    }

    @Override
    public void getTicketDetails(Context ctx, Map<String, String> keyword, final String tag) {
        client.getTicketDetails(keyword, new Callback<TicketDetailsResponse>() {
            @Override
            public void success(TicketDetailsResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }
        });
    }

    @Override
    public void getGroupList(Context ctx, Map<String, String> keyword, final String tag) {
        client.getGroupList(keyword, new Callback<GroupResponse>() {
            @Override
            public void success(GroupResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }
        });
    }


    @Override
    public void getCategoryList(Context ctx, Map<String, String> keyword, final String tag) {
        client.getCategoryList(keyword, new Callback<CategoryResponse>() {
            @Override
            public void success(CategoryResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }
        });
    }

    @Override
    public void getSubCategoryList(Context ctx, Map<String, String> keyword, final String tag) {
        client.getSubCategoryList(keyword, new Callback<SubCategoryResponse>() {
            @Override
            public void success(SubCategoryResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }
        });
    }

    @Override
    public void getStatusList(Context ctx, Map<String, String> keyword, final String tag) {
        client.getStatusList(keyword, new Callback<StatusResponse>() {
            @Override
            public void success(StatusResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }
        });
    }

    @Override
    public void getPriorityList(Context ctx, Map<String, String> keyword, final String tag) {
        client.getPriorityList(keyword, new Callback<PriorityResponse>() {
            @Override
            public void success(PriorityResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }
        });
    }

    @Override
    public void getAdminUserList(Context ctx, Map<String, String> keyword, final String tag) {
        client.getAdminUserList(keyword, new Callback<AdminUserResponse>() {
            @Override
            public void success(AdminUserResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }
        });
    }

    @Override
    public void getEmployeeList(Context ctx, Map<String, String> keyword, final String tag) {
        client.getEmployeeList(keyword, new Callback<EmployeeResponse>() {
            @Override
            public void success(EmployeeResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }
        });
    }

    @Override
    public void updateTicket(Context ctx, Map<String, String> keyword, final String tag) {
        client.updateTicket(keyword, new Callback<UpdateTicketResponse>() {
            @Override
            public void success(UpdateTicketResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }
        });
    }

    @Override
    public void deleteComment(Context ctx, Map<String, String> keyword, final String tag) {
        client.deleteComment(keyword, new Callback<DeleteCommentResponse>() {
            @Override
            public void success(DeleteCommentResponse arg0, Response arg1) {
                onResponse(arg0, tag);
            }

            @Override
            public void failure(RetrofitError error) {
                onErrorResponse(error, tag);
            }
        });
    }
}