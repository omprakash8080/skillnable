
package com.skill.api;

import com.skill.model.*;

import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.client.OkClient;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.QueryMap;

public class ApiClient {

//	private static String BASE = "http://private-5f3100-itsupport.apiary-mock.com";
	private static String BASE = "http://support.intelliswift.co.in/services";

	private static ApiService apiService;

	public interface ApiService {
		@POST("/authenticate")
		void login(@Body Map<String, String> params, Callback<LoginResponse> callback);
		@GET("/getTickets")
		void getTickets(@QueryMap Map<String, String> params, Callback<TicktesResponse> callback);
		@GET("/getTicketDetails")
		void getTicketDetails(@QueryMap Map<String, String> params, Callback<TicketDetailsResponse> callback);
		@GET("/getGroupList")
		void getGroupList(@QueryMap Map<String, String> params, Callback<GroupResponse> callback);
		@GET("/getCategoryList")
		void getCategoryList(@QueryMap Map<String, String> params, Callback<CategoryResponse> callback);
		@GET("/getSubCategoryList")
		void getSubCategoryList(@QueryMap Map<String, String> params, Callback<SubCategoryResponse> callback);
		@GET("/getStatusList")
		void getStatusList(@QueryMap Map<String, String> params, Callback<StatusResponse> callback);
		@GET("/getPriorityList")
		void getPriorityList(@QueryMap Map<String, String> params, Callback<PriorityResponse> callback);
		@GET("/getAdminUserList")
		void getAdminUserList(@QueryMap Map<String, String> params, Callback<AdminUserResponse> callback);
		@GET("/getUserList")
		void getEmployeeList(@QueryMap Map<String, String> params, Callback<EmployeeResponse> callback);
		@POST("/updateTicket")
		void updateTicket(@Body Map<String, String> params, Callback<UpdateTicketResponse> callback);
		@DELETE("/deleteComment")
		void deleteComment(@QueryMap Map<String, String> params, Callback<DeleteCommentResponse> callback);



	}
	
	public static void init() {
		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(
				BASE)
				.setClient(new OkClient())
				.setLogLevel(LogLevel.FULL)
				.build();
        
		apiService = restAdapter.create(ApiService.class);
	}

	public static ApiService getInstance() {
		return apiService;
	}
	
//	public static class MyErrorHandler implements ErrorHandler {
//	      @Override public Throwable handleError(RetrofitError cause) {
//	        Response r = cause.getResponse();
//	        if (r != null && r.getStatus() == 401) {
//	          return new Throwable(cause);
//	        }
//	        return cause;
//	      }
//	    }
}
