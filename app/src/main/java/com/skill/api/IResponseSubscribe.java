package com.skill.api;

public interface IResponseSubscribe {

	 public void onResponse(Object response, String tag);

	 public void onErrorResponse(Exception error, String tag);

}
