package com.skill.api;

import android.content.Context;

import java.util.HashMap;

public class IOCContainer {

	private static IOCContainer instance;
	public ResponsePublisher publisher;
	private Context context;

	private final static HashMap<ObjectName, Object> objectContainer = new HashMap<ObjectName, Object>();

	public enum ObjectName {
		ALL_API_SERVICE;
	}

	public Context getContext() {
		return context;
	}

	private IOCContainer() {
		publisher = new ResponsePublisher();
	}

	public static IOCContainer getInstance() {

		if (instance == null) {
			instance = new IOCContainer();
		}
		return instance;
	}

	public void init(Context context) {
		this.context = context;
	}

	public IBaseFacade getObject(ObjectName name, String tag) {

		IBaseFacade object = (IBaseFacade) objectContainer.get(name);

		if (object == null) {

			switch (name) {

			case ALL_API_SERVICE:

				object = new AllApiFacade(publisher);
				break;
			}
			objectContainer.put(name, object);
		}
		object.setTag(tag);

		return object;
	}
}
