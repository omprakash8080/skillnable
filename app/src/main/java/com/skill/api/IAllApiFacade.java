package com.skill.api;

import android.content.Context;

import java.util.Map;

public interface IAllApiFacade extends IBaseFacade {
	
	void userLogin(Context ctx, Map<String, String> keyword, String tag);
	void getAllTickets(Context ctx, Map<String, String> keyword, String tag);
	void getTicketDetails(Context ctx, Map<String, String> keyword, String tag);
	void getGroupList(Context ctx, Map<String, String> keyword, String tag);
	void getCategoryList(Context ctx, Map<String, String> keyword, String tag);
	void getSubCategoryList(Context ctx, Map<String, String> keyword, String tag);
	void getStatusList(Context ctx, Map<String, String> keyword, String tag);
	void getPriorityList(Context ctx, Map<String, String> keyword, String tag);
	void getAdminUserList(Context ctx, Map<String, String> keyword, String tag);
	void getEmployeeList(Context ctx, Map<String, String> keyword, String tag);
	void updateTicket(Context ctx, Map<String, String> keyword, String tag);
	void deleteComment(Context ctx, Map<String, String> keyword, String tag);


}
