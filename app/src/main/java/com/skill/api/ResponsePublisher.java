package com.skill.api;

import java.util.concurrent.CopyOnWriteArrayList;

public class ResponsePublisher implements IResponseSubscribe {

	private CopyOnWriteArrayList<IResponseSubscribe> responseObservers;

	public ResponsePublisher() {
		responseObservers = new CopyOnWriteArrayList<IResponseSubscribe>();
	}

	public void registerResponseSubscribe(IResponseSubscribe responseObserver) {
		responseObservers.add(responseObserver);

	}

	public void unregisterResponseSubscribe(IResponseSubscribe responseObserver) {
		responseObservers.remove(responseObserver);
	}

	@Override
	public void onResponse(Object response, String tag) {
		for (IResponseSubscribe observer : responseObservers) {
			observer.onResponse(response, tag);
		}
	}

	@Override
	public void onErrorResponse(Exception error, String tag) {
		for (IResponseSubscribe observer : responseObservers) {
			observer.onErrorResponse(error, tag);
		}
	}

}
