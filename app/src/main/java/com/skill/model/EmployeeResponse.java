package com.skill.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omprakash on 29/2/16.
 */
public class EmployeeResponse {

    private List<Datum> data = new ArrayList<>();
    private String status;
    private String errorCode;
    private String message;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
