package com.skill.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by omprakash on 13/5/16.
 */
public class Message {

    private Integer chatCount = 0;

    private String body;
    private String username;

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String Name;

    public void setBody(String body) {
        this.body = body;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    @SerializedName("image-url")
    private String imageUrl;
    @SerializedName("message-time")
    private String messageTime;

    public String getBody() {
        return body;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public Integer getChatCount() {
        return chatCount;
    }

    public void setChatCount(Integer chatCount) {
        this.chatCount = chatCount;
    }
}
