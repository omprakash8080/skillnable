package com.skill.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omprakash on 17/5/16.
 */
public class DeleteCommentResponse {
    private List<Object> data = new ArrayList<Object>();
    private String status;
    private String errorCode;
    private String message;

    public List<Object> getData() {
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
