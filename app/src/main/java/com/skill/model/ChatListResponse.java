package com.skill.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omprakash on 13/5/16.
 */
public class ChatListResponse {


    private Integer count;
    private List<Message> messages = new ArrayList<>();

    public Integer getCount() {
        return count;
    }
    public List<Message> getMessages() {
        return messages;
    }



}
