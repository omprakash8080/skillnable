package com.skill.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by omprakash on 29/2/16.
 */
public class Datum {
    private String userid;
    private String ticketId;
    private String title;
    private String group;
    private String status;
    private String creator;
    private String lastActivity;
    private String timeTaken;
    private String rating;
    private String technician;
    private String details;
    private String category;
    private String subCategory;
    private String priority;
    private Integer id;
    private String text;
    private String username;
    private String creatorEmailId;
    private String technicianEmailId;
    private List<Comment> comments = new ArrayList<Comment>();

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getTechnicianEmailId() {
        return technicianEmailId;
    }

    public void setTechnicianEmailId(String technicianEmailId) {
        this.technicianEmailId = technicianEmailId;
    }

    public String getCreatorEmailId() {
        return creatorEmailId;
    }

    public void setCreatorEmailId(String creatorEmailId) {
        this.creatorEmailId = creatorEmailId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getTechnician() {
        return technician;
    }

    public void setTechnician(String technician) {
        this.technician = technician;
    }

    /**
     * @return The ticketId
     */
    public String getTicketId() {
        return ticketId;
    }

    /**
     * @param ticketId The ticketId
     */
    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The group
     */
    public String getGroup() {
        return group;
    }

    /**
     * @param group The group
     */
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return The creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * @param creator The creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * @return The lastActivity
     */
    public String getLastActivity() {
        return lastActivity;
    }

    /**
     * @param lastActivity The lastActivity
     */
    public void setLastActivity(String lastActivity) {
        this.lastActivity = lastActivity;
    }

    /**
     * @return The timeTaken
     */
    public String getTimeTaken() {
        return timeTaken;
    }

    /**
     * @param timeTaken The timeTaken
     */
    public void setTimeTaken(String timeTaken) {
        this.timeTaken = timeTaken;
    }

    /**
     * @return The rating
     */
    public String getRating() {
        return rating;
    }

    /**
     * @param rating The rating
     */
    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUserid() {
        return userid;
    }

    /**
     * @param userid The userid
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

}
