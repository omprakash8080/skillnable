package com.skill.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skill.R;
import com.skill.model.Datum;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by omprakash on 29/2/16.
 */


public class TicketsRecyclerViewAdapter extends RecyclerView.Adapter<TicketsRecyclerViewAdapter.ViewHolder> {

    private static String TAG = "TicketsRecyclerViewAdapter";
    private ArrayList<Datum> list;
    private Context context;
    private static ItemClickListener myClickListener;
    private boolean isLoading = false;

    public TicketsRecyclerViewAdapter(Context context, ArrayList<Datum> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.cardview_ticket_row, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Datum datum = list.get(position);

        holder.label.setText(datum.getTitle());
        holder.dateTime.setText(datum.getCreator());
        holder.status.setText(datum.getStatus());
        holder.txt_technician.setText(datum.getTechnician());

        holder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myClickListener != null) {
                    myClickListener.onItemClick(position, v);
                }
            }
        });

    }

    public void addItem(Datum dataObj, int index) {
        list.add(index, dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index) {
        list.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.textView)
        public TextView label;
        @Bind(R.id.textView2)
        public TextView dateTime;
        @Bind(R.id.textView3)
        public TextView status;
        @Bind(R.id.card_view)
        CardView card_view;
        @Bind(R.id.txt_technician)
        public TextView txt_technician;



        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static void setItemClickListener(ItemClickListener myClickListener) {
        TicketsRecyclerViewAdapter.myClickListener = myClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(int position, View v);
    }

}
