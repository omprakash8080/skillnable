package com.skill.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.skill.R;
import com.skill.model.Comment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by omprakash on 29/2/16.
 */


public class CommentsRecyclerViewAdapter extends RecyclerView.Adapter<CommentsRecyclerViewAdapter.ViewHolder> {

    private static String TAG = "TicketsRecyclerViewAdapter";
    private ArrayList<Comment> list;
    private Context context;
    private static ItemClickListener myClickListener;
    private boolean isLoading = false;

    public CommentsRecyclerViewAdapter(Context context, ArrayList<Comment> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(context).inflate(R.layout.cardview_comment_row, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Comment comment = list.get(position);

        holder.label.setText(comment.getComment());
        holder.dateTime.setText(comment.getCommenter());
//        holder.status.setText(comment.getStatus());
        holder.txt_technician.setText(comment.getDate());

        holder.deleteComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (myClickListener != null) {
                    myClickListener.onItemClick(position, comment.getCommentId(), v);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public void removeItem(int position) {
        if (list == null && list.size() < position) {
            return;
        }

        list.remove(position);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.textView)
        public TextView label;
        @Bind(R.id.textView2)
        public TextView dateTime;
        @Bind(R.id.textView3)
        public TextView status;
        @Bind(R.id.card_view)
        CardView card_view;
        @Bind(R.id.txt_technician)
        public TextView txt_technician;
        @Bind(R.id.delete_comment)
        public Button deleteComment;



        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static void setItemClickListener(ItemClickListener myClickListener) {
        CommentsRecyclerViewAdapter.myClickListener = myClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(int position, int commentId, View v);
    }

}
