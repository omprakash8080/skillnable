package com.skill.helper;

import android.content.pm.ActivityInfo;
import android.os.Build;

import com.skill.fragment.BaseFragment;
import com.skill.fragment.CommentListFragment;
import com.skill.fragment.CoursesFragment;
import com.skill.fragment.FeedbackFragment;
import com.skill.fragment.HomeFragment;
import com.skill.fragment.LoginFragment;
import com.skill.fragment.TicketDetailsFragment;
import com.skill.fragment.TicketsFragment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AndroidUtils {

    private static boolean isTablet;
    public static int ORIENTATION_FLAG;
    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";

    public static void init(String screenType) {
        if (screenType.equalsIgnoreCase("tablet")) {
            isTablet = true;
        }
        setOrientation();
    }

    private static void setOrientation() {

        if (isTablet) {
            ORIENTATION_FLAG = ActivityInfo.SCREEN_ORIENTATION_USER;
        } else {
            ORIENTATION_FLAG = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        }
    }

    public static boolean isTablet() {
        return isTablet;
    }

    public static boolean isJellyBeanOrHigher() {

        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }

    public static boolean isLollipopOrHigher(){
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static class FragmentTag {
        public static final int LOGINFRAGMENT = 0, HOMEFRAGMENT = 1,
                TICKET_DETAILS_FRAGMENT = 2, FEEDBACK_FRAGMENT = 3,
                COMMENTS_LIST_FRAGMENT=4,
        COURSE_FRAGMENT = 5;

//        public static int valueOf(String s) {
//
//            if (s.trim().equals("USERDETAIL")) {
//                return 38;
//            } else if (s.trim().equals("LOGIN")) {
//                return 20;
//            }
//
//            return -1;
//        }

    }

    public static BaseFragment getFragment(int fragmentTag) {
        BaseFragment fragment = null;

        switch (fragmentTag) {
            case FragmentTag.LOGINFRAGMENT:
                fragment = new LoginFragment();
                break;

            case FragmentTag.COURSE_FRAGMENT:
                fragment = new CoursesFragment();
                break;

            case FragmentTag.HOMEFRAGMENT:
                fragment = new HomeFragment();
                break;


            case FragmentTag.TICKET_DETAILS_FRAGMENT:
                fragment = new TicketDetailsFragment();
                break;

            case FragmentTag.FEEDBACK_FRAGMENT:
                fragment = new FeedbackFragment();
                break;

            case FragmentTag.COMMENTS_LIST_FRAGMENT:
                fragment = new CommentListFragment();
                break;

            default:
                break;

        }
        return fragment;
    }

    //Unused method

    /*public static void makeLinkClickable(SpannableStringBuilder strBuilder,
                                         final URLSpan span) {
        int start = strBuilder.getSpanStart(span);
        int end = strBuilder.getSpanEnd(span);
        int flags = strBuilder.getSpanFlags(span);
        // URLSpan[] spans = strBuilder.getSpans(0, strBuilder.length(),
        // URLSpan.class);

        ClickableSpan clickable = new ClickableSpan() {
            public void onClick(View view) {
                // Do something with span.getURL() to handle the link click...
                HtmlLinkClickEvent event = new HtmlLinkClickEvent();
                event.url = span.getURL();
                BusProvider.getInstance().post(event);

            }
        };

        strBuilder.setSpan(clickable, start, end, flags);
        URLSpan span1 = new URLSpanNoUnderline(span.getURL());
        strBuilder.setSpan(span1, start, end, flags);
        strBuilder.removeSpan(span);

    }*/

   /* public static void setHTMLText(TextView text, String html) {
        CharSequence sequence = Html.fromHtml(html);
        SpannableStringBuilder strBuilder = new SpannableStringBuilder(sequence);
        URLSpan[] urls = strBuilder.getSpans(0, sequence.length(),
                URLSpan.class);
        for (URLSpan span : urls) {
            makeLinkClickable(strBuilder, span);
        }
        text.setText(strBuilder);
    }*/

    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isPasswordValid(final String password) {
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();

    }


}
