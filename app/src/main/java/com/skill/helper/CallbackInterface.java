package com.skill.helper;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public interface CallbackInterface {

    public void switchtoFragment(int tag, boolean addtoBackStack,
                                 int layout_id, boolean animate);

    public void switchtoFragment(int tag, boolean addtoBackStack,
                                 int layout_id, boolean animate, Bundle bundle);

    public void switchFragment(Fragment fragment, boolean addtoBackStack,
                               int layout_id, boolean animate);

}
