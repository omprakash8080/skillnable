package com.skill.helper;

/**
 * Created by omprakash on 29/2/16.
 */
public interface Constants {

    String SUCCESS = "success";
    String IS_LOGIN = "is_login";
    String USER_DATA = "UserData";
    String STATUS_ID = "status_id";
    String TICKETS_TITLE = "tickets_title";
    String LoginResponse = "LoginResponse";

    String QUERY = "query";
    String USER_LOGGED_IN = "USER_LOGGED_IN";
}
