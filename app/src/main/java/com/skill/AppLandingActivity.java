package com.skill;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.skill.helper.AndroidUtils;
import com.skill.helper.Constants;
import com.skill.helper.TinyDB;

import butterknife.Bind;
import butterknife.ButterKnife;


public class AppLandingActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private int currentPos;
    private Intent intent;
    private Context context;
    ActionBarDrawerToggle drawerToggle;
    private static final long DRAWER_CLOSE_DELAY_MS = 250;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @Bind(R.id.navigationView)
    NavigationView mNavigationView;
    TinyDB tinyDB;

    TextView userName;
    TextView emailId;
    ImageView userIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tinyDB = TinyDB.getInstance(this);
        tinyDB.putString(Constants.STATUS_ID, ""); //All
        if (!tinyDB.getBoolean(IS_LOGIN)) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
        setUpHomeButton();

        drawerToggle = setupDrawerToggle();
        mDrawer.setDrawerListener(drawerToggle);

        View header  = mNavigationView.getHeaderView(0);
        userName = (TextView) header.findViewById(R.id.username);
        emailId = (TextView) header.findViewById(R.id.locationName);
        userIcon = (ImageView) header.findViewById(R.id.profileImage);


        /*com.skill.model.LoginResponse loginResponse = (LoginResponse)tinyDB.getObject(Constants.LoginResponse, LoginResponse.class);

        Data userdata = loginResponse.getData();

        userName.setText(userdata.getName());
        emailId.setText(userdata.getEmailId());

        Picasso.with(context).load(userdata.getUserImage()).into(userIcon);

        userName.setText(userdata.getName());
*/

//        TextView text = (TextView) header.findViewById(R.id.textView);
        mNavigationView.setNavigationItemSelectedListener(this);

//        View headerLayout = mNavigationView.getH


        /*intent = getIntent();
        int pos = currentPos = intent.getIntExtra("position", 0);
        mNavigationView.getMenu().getItem(pos).setCheckable(true).setChecked(true);*/

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.HOMEFRAGMENT), false, R.id.fragmentContainer, false);
    }

    void setUpHomeButton() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.menu_icon);

    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       /* getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;*/

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchManager searchManager = (SearchManager) AppLandingActivity.this.getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(AppLandingActivity.this.getComponentName()));
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        final int id = menuItem.getItemId();

        tinyDB = TinyDB.getInstance(context);
        tinyDB.putBoolean(IS_LOGIN, false);

        menuItem.setChecked(true);

        mDrawer.closeDrawer(GravityCompat.START);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (id == R.id.item_home) {
                    tinyDB.putString(Constants.STATUS_ID, "1");
                    tinyDB.putString(Constants.TICKETS_TITLE, "SkillAndAble");//open
                    switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.HOMEFRAGMENT), false, R.id.fragmentContainer, false);
                } else if (id == R.id.item_courses) {

                    tinyDB.putString(Constants.STATUS_ID, "2"); //Close
                    tinyDB.putString(Constants.TICKETS_TITLE, "Courses");//open
                    switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.COURSE_FRAGMENT), false, R.id.fragmentContainer, false);

                } else if (id == R.id.item_logout) {
                    signOut();

                } else {

                }
            }
        }, DRAWER_CLOSE_DELAY_MS);


        return true;
    }


    private void signOut() {

        tinyDB = TinyDB.getInstance(context);
        tinyDB.putBoolean(IS_LOGIN, false);

        startActivity(new Intent(context, LoginActivity.class));
        Toast.makeText(context, "Signing out.", Toast.LENGTH_SHORT).show();
        finish();
    }

}
