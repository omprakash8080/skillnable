package com.skill;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.widget.ImageView;

import com.skill.helper.TinyDB;


/**
 * Created by abc on 16-Oct-15.
 */
public class SplashActivity extends BaseActivity {

    private ImageView img;
    private ConnectivityManager cm;
    private AlertDialog alertDNew;
    static String TAG = "Splash";
    TinyDB tinyDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        tinyDB = TinyDB.getInstance(this);
        img = (ImageView) findViewById(R.id.image);
//        DisplayMetrics size = CommonTasks.getScreenSize(SplashActivity.this);
//        int height = size.heightPixels;
//        int width = size.widthPixels;
//        img.getLayoutParams().height = (int) (height * 0.16);
//        img.getLayoutParams().width = (int) (width * 0.49);

        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {

//            getCountryList();
//            executorService.submit(new Thread(new ThreadGetToken()));
//            executorService.submit(new Thread(new ThreadGetCountryList()));
//            executorService.submit(new Thread(new ThreadGetSourceOfFundId()));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    init();
                }
            }).start();
        } else {
            showAlertDialogNew();
        }
    }

    private void init() {

        try {
            Thread.sleep(3000);
            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    boolean userLoggedIn = tinyDB.getBoolean(IS_LOGIN);
                    if (userLoggedIn) {
                        startActivity(new Intent(SplashActivity.this, AppLandingActivity.class));
                    } else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    }
                    finish();
                }
            });

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void showAlertDialogNew() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle("No Network Access");
        builder.setMessage("Do you want to check your internet settings.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                return;
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SplashActivity.this.finish();
                return;
            }
        });
        alertDNew = builder.create();
        alertDNew.show();

    }

}
