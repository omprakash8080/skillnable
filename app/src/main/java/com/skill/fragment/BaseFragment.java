package com.skill.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;

import com.skill.R;
import com.skill.api.IOCContainer;
import com.skill.api.IResponseSubscribe;
import com.skill.helper.Constants;

public abstract class BaseFragment extends Fragment implements IResponseSubscribe, Constants {

    protected Activity activityContext;
    ProgressBar progressBar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activityContext = (Activity)context;
        IOCContainer.getInstance().publisher.registerResponseSubscribe(this);

        progressBar = (ProgressBar) getActivity().findViewById(R.id.progressbar);

    }

    void progressBarVisibility(boolean isVisible) {

        if (progressBar == null) {
            return;
        }

        if (isVisible) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.
                    setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        IOCContainer.getInstance().publisher.unregisterResponseSubscribe(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void switchFragment(Fragment fragment, boolean addtoBackStack,
                               int layout_id, boolean animate) {

        if (getActivity() != null) {
            if (!getActivity().isFinishing()) {
                FragmentTransaction fragmentTransaction = getActivity()
                        .getSupportFragmentManager().beginTransaction();
                if (animate) {

                   /* fragmentTransaction.setCustomAnimations(R.anim.slide_up,
                            R.anim.slide_down,
                            R.anim.slide_up,
                            R.anim.slide_down);*/
//					ApplyAnimation.toFragment(fragmentTransaction,
//							AnimationEffect.TRANSALTE_IN,
//							AnimationEffect.TRANSALTE_OUT);
                }
                fragmentTransaction.replace(layout_id, fragment,
                        fragment.getTag());
                if (addtoBackStack) {
                    fragmentTransaction.addToBackStack(fragment.getTag());
                }

                fragmentTransaction.commit();
            }
        }
    }

    public void hideKeyboardfromFragment() {
        if (getActivity() != null) {
            View view = getActivity().getCurrentFocus();
            if (view != null && view.getWindowToken() != null) {
                InputMethodManager inputManager = (InputMethodManager) getActivity()
                        .getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }
}
