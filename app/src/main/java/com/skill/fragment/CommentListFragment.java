package com.skill.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.skill.R;
import com.skill.adapter.CommentsRecyclerViewAdapter;
import com.skill.api.IAllApiFacade;
import com.skill.api.IOCContainer;
import com.skill.helper.CallbackInterface;
import com.skill.helper.TinyDB;
import com.skill.model.Comment;
import com.skill.model.DeleteCommentResponse;
import com.skill.model.TicketDetailsResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CommentListFragment extends BaseFragment implements CommentsRecyclerViewAdapter.ItemClickListener {

    public static String TAG = "CommentListFragment";
    private static Context context;
    static IAllApiFacade allApiFacade;
    CommentsRecyclerViewAdapter adapter;
    CallbackInterface callbackInterface;
    private ArrayList<Comment> commentsList;
    LinearLayoutManager linearLayoutManager;
    int itemPosition = 0;
    int pageNumber = 0;

    @Bind(R.id.my_recycler_view)
    RecyclerView myRecyclerView;

    TinyDB tinyDB;

    @Bind(R.id.no_item)
    TextView no_item_text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        commentsList = new ArrayList<>();
        adapter = new CommentsRecyclerViewAdapter(activityContext, commentsList);
        adapter.setItemClickListener(this);
        tinyDB = new TinyDB(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        callbackInterface = (CallbackInterface) getActivity();

        View layout = inflater.inflate(R.layout.fragment_comment_list, container,
                false);
        ButterKnife.bind(this, layout);

        intializeComponent();
        toolbarAdjusment();


        TicketDetailsResponse detailsResponse = (TicketDetailsResponse) tinyDB.getObject("TicketDetailsResponse", TicketDetailsResponse.class);

        if (detailsResponse.getData().size() > 0) {

            List<Comment> commentList = detailsResponse.getData().get(0).getComments();
            commentsList.addAll(commentList);
            adapter.notifyDataSetChanged();
            no_item_text.setVisibility(View.INVISIBLE);
        }else {
            no_item_text.setVisibility(View.VISIBLE);
        }

        return layout;
    }

    void toolbarAdjusment() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("Comments");

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);
    }

    void intializeComponent() {
        context = getActivity();
        commentsList.clear();
        linearLayoutManager = new LinearLayoutManager(activityContext);
        myRecyclerView.setLayoutManager(linearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(adapter);

        /*myRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // do something...
                pageNumber++;
                getAllTickets();
            }
        });
*/
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResponse(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

        if (response instanceof DeleteCommentResponse) {
            progressBarVisibility(false);
            DeleteCommentResponse commentResponse = (DeleteCommentResponse) response;

            if (commentResponse.getStatus().equalsIgnoreCase(SUCCESS)) {
                Toast.makeText(context, commentResponse.getMessage(), Toast.LENGTH_SHORT).show();
                adapter.removeItem(itemPosition);
            }
        }

    }

    @Override
    public void onErrorResponse(Exception error, String tag) {
        progressBarVisibility(false);

    }

    @Override
    public void onItemClick(int position, int commentId, View v) {

        itemPosition = position;
        progressBarVisibility(true);

        allApiFacade = (IAllApiFacade) IOCContainer.getInstance().getObject(IOCContainer.ObjectName.ALL_API_SERVICE, TAG);
        Map<String, String> params = new HashMap<String, String>();
        params.put("commentid", commentId+"");

        allApiFacade.deleteComment(context,params, TAG);
    }
}