package com.skill.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperActivityToast;
import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;
import com.skill.AppLandingActivity;
import com.skill.R;
import com.skill.api.IAllApiFacade;
import com.skill.helper.CallbackInterface;
import com.skill.helper.TinyDB;
import com.skill.model.LoginResponse;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FeedbackFragment extends BaseFragment {

    public static String TAG = "LoginFragment";

    CallbackInterface callbackInterface;
    static IAllApiFacade allApiFacade;
    private static Context context;
    View layout;

    @Bind(R.id.txt_default_fd_msg)
    TextView txt_default_fd_msg;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        layout = inflater.inflate(R.layout.fragment_feedback, container,
                false);
        ButterKnife.bind(this, layout);
        intializeComponent();
        toolbarAdjusment();
        hideKeyboardfromFragment();
        return layout;
    }

    void intializeComponent(){
        callbackInterface = (CallbackInterface) getActivity();
        context = getActivity();

//        String str = ""
//        txt_default_fd_msg.setText(Html.fromHtml());
    }
    void toolbarAdjusment(){
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("Feedback");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResponse(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

        if (response instanceof com.skill.model.LoginResponse) {
            progressBarVisibility(false);
//            toggleVisibility(false, mProgressBar);
            LoginResponse loginResponse = (LoginResponse) response;
            if (loginResponse.getStatus().equalsIgnoreCase(SUCCESS)) {

                TinyDB tinyDB = TinyDB.getInstance(getActivity());
                tinyDB.putBoolean(IS_LOGIN, true);
                tinyDB.putObject(USER_DATA, loginResponse.getData());

//                Toast.makeText(context, loginResponse.getData().get(0).getUserid(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), AppLandingActivity.class);
                startActivity(intent);
                getActivity().finish();


            }else {

                Snackbar snack =  Snackbar.make(layout, loginResponse.getMessage(), Snackbar.LENGTH_SHORT);

                ViewGroup group = (ViewGroup) snack.getView();
                group.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));

                snack.show();
                       // .setAction(action message, click listener)
//                .show();

                final Style customStyle = new Style();
                customStyle.animations = SuperToast.Animations.FLYIN;
                customStyle.background = SuperToast.Background.GREEN;
                customStyle.textColor = Color.WHITE;
                customStyle.buttonTextColor = Color.LTGRAY;
                customStyle.dividerColor = Color.WHITE;

                final SuperActivityToast superActivityToast = new SuperActivityToast(getActivity(), customStyle);
                superActivityToast.setDuration(SuperToast.Duration.SHORT);
                superActivityToast.setText(loginResponse.getMessage());
                superActivityToast.show();
            }
        }
    }

    @Override
    public void onErrorResponse(Exception error, String tag) {
        progressBarVisibility(false);
    }
}