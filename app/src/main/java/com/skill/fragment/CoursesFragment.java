package com.skill.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skill.R;
import com.skill.adapter.TicketsRecyclerViewAdapter;
import com.skill.helper.CallbackInterface;

import butterknife.ButterKnife;

public class CoursesFragment extends BaseFragment implements TicketsRecyclerViewAdapter.ItemClickListener {

    public static String TAG = "TicketsFragment";
    private static Context context;
    CallbackInterface callbackInterface;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        callbackInterface = (CallbackInterface) getActivity();
        context = getActivity();
        View layout = inflater.inflate(R.layout.fragment_courses, container,
                false);
        ButterKnife.bind(this, layout);

        toolbarAdjusment();


        return layout;
    }

    void toolbarAdjusment() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("Courses");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResponse(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

    }

    @Override
    public void onErrorResponse(Exception error, String tag) {
        progressBarVisibility(false);

    }

    @Override
    public void onItemClick(int position, View v) {

    }
}