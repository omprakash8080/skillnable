package com.skill.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.github.johnpersano.supertoasts.SuperActivityToast;
import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;
import com.skill.AppLandingActivity;
import com.skill.R;
import com.skill.api.IAllApiFacade;
import com.skill.api.IOCContainer;
import com.skill.helper.AndroidUtils;
import com.skill.helper.CallbackInterface;
import com.skill.helper.TinyDB;
import com.skill.model.LoginResponse;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginFragment extends BaseFragment {

    public static String TAG = "LoginFragment";

    CallbackInterface callbackInterface;
    @Bind(R.id.input_email)
    EditText inputEmail;
    @Bind(R.id.input_password)
    EditText inputPassword;
    @Bind(R.id.btn_login)
    AppCompatButton btnLogin;

    static IAllApiFacade allApiFacade;
    private static Context context;
    View layout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        layout = inflater.inflate(R.layout.fragment_login, container,
                false);
        ButterKnife.bind(this, layout);
        intializeComponent();
        toolbarAdjusment();
        hideKeyboardfromFragment();
        return layout;
    }

    void intializeComponent() {
        callbackInterface = (CallbackInterface) getActivity();
        context = getActivity();
//        inputEmail.setText("support@intelliswift.co.in");
//        inputPassword.setText("support@54321$");
    }

    void toolbarAdjusment() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.btn_login)
    public void performLogin(View view) {

        TinyDB tinyDB = TinyDB.getInstance(getActivity());
        tinyDB.putBoolean(IS_LOGIN, true);
        Intent intent = new Intent(getActivity(), AppLandingActivity.class);
        startActivity(intent);
        getActivity().finish();

        /*String email = inputEmail.getText().toString().trim();
        String password = inputPassword.getText().toString().trim();

        if (checkValidation()) {
            hideKeyboardfromFragment();
            Map<String, String> loginParams = new HashMap<String, String>();

            loginParams.put("email", email);
            loginParams.put("password", password);
            loginParams.put("isGoogleAuthenticate", "0");
            allApiFacade = (IAllApiFacade) IOCContainer.getInstance().getObject(IOCContainer.ObjectName.ALL_API_SERVICE, TAG);
            allApiFacade.userLogin(context, loginParams, TAG);
            progressBarVisibility(true);
        }
*/

    }

    private boolean checkValidation() {
        boolean ret = true;

        String email = inputEmail.getText().toString().trim();
        String password = inputPassword.getText().toString().trim();

        if (!AndroidUtils.isEmailValid(email) || email.length() > 50) {
            inputEmail.setError("Email Invalid");
            ret = false;

        } /*else if (!AndroidUtils.isPasswordValid(password)) {
            inputPassword.setError("Password must be 6 characters in length and should contain atleast 1 digit and 1 uppercase letter.");
            ret = false;
        }*/
        return ret;
    }

    @Override
    public void onResponse(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

        if (response instanceof com.skill.model.LoginResponse) {
            progressBarVisibility(false);
//            toggleVisibility(false, mProgressBar);
            LoginResponse loginResponse = (LoginResponse) response;
            if (loginResponse.getStatus().equalsIgnoreCase(SUCCESS)) {

                TinyDB tinyDB = TinyDB.getInstance(getActivity());
                tinyDB.putBoolean(IS_LOGIN, true);
                tinyDB.putObject(USER_DATA, loginResponse.getData());
                tinyDB.putObject(LoginResponse, loginResponse);


//                Toast.makeText(context, loginResponse.getData().get(0).getUserid(), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), AppLandingActivity.class);
                startActivity(intent);
                getActivity().finish();


            } else {

               /* Snackbar snack =  Snackbar.make(layout, loginResponse.getMessage(), Snackbar.LENGTH_SHORT);
                ViewGroup group = (ViewGroup) snack.getView();
                group.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorAccent));

                snack.show();*/
                // .setAction(action message, click listener)
//                .show();

                final Style customStyle = new Style();
                customStyle.animations = SuperToast.Animations.FLYIN;
                customStyle.background = SuperToast.Background.GREEN;
                customStyle.textColor = Color.WHITE;
                customStyle.buttonTextColor = Color.LTGRAY;
                customStyle.dividerColor = Color.WHITE;

                final SuperActivityToast superActivityToast = new SuperActivityToast(getActivity(), customStyle);
                superActivityToast.setDuration(SuperToast.Duration.SHORT);
                superActivityToast.setText(loginResponse.getMessage());
                superActivityToast.show();
            }
        }
    }

    @Override
    public void onErrorResponse(Exception error, String tag) {
        progressBarVisibility(false);
    }
}