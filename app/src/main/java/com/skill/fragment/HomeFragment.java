package com.skill.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skill.DetailsActivity;
import com.skill.R;
import com.skill.adapter.TicketsRecyclerViewAdapter;
import com.skill.api.IAllApiFacade;
import com.skill.api.IOCContainer;
import com.skill.helper.CallbackInterface;
import com.skill.helper.Constants;
import com.skill.helper.EndlessRecyclerOnScrollListener;
import com.skill.helper.TinyDB;
import com.skill.model.Datum;
import com.skill.model.TicktesResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment implements TicketsRecyclerViewAdapter.ItemClickListener {

    public static String TAG = "TicketsFragment";
    private static Context context;
    CallbackInterface callbackInterface;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        callbackInterface = (CallbackInterface) getActivity();
        context = getActivity();
        View layout = inflater.inflate(R.layout.fragment_home, container,
                false);
        ButterKnife.bind(this, layout);

        toolbarAdjusment();


        return layout;
    }

    void toolbarAdjusment() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("SkillAndAble");
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResponse(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

    }

    @Override
    public void onErrorResponse(Exception error, String tag) {
        progressBarVisibility(false);

    }

    @Override
    public void onItemClick(int position, View v) {

    }
}