package com.skill.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.skill.R;
import com.skill.adapter.TicketsRecyclerViewAdapter;
import com.skill.api.IAllApiFacade;
import com.skill.api.IOCContainer;
import com.skill.helper.AndroidUtils;
import com.skill.helper.AnimUtils;
import com.skill.helper.CallbackInterface;
import com.skill.helper.TinyDB;
import com.skill.model.AdminUserResponse;
import com.skill.model.CategoryResponse;
import com.skill.model.Data;
import com.skill.model.Datum;
import com.skill.model.EmployeeResponse;
import com.skill.model.GroupResponse;
import com.skill.model.PriorityResponse;
import com.skill.model.StatusResponse;
import com.skill.model.SubCategoryResponse;
import com.skill.model.TicketDetailsResponse;
import com.skill.model.UpdateTicketResponse;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TicketDetailsFragment extends BaseFragment implements View.OnClickListener, TextWatcher {

    CallbackInterface callbackInterface;
    static IAllApiFacade allApiFacade;

    private static Context context;
    TicketsRecyclerViewAdapter adapter;
    private ArrayList<Datum> ticketList;
    LinearLayoutManager linearLayoutManager;

    @Bind(R.id.layout_comment)
    LinearLayout layoutCommentView;

    @Bind(R.id.chk_add_comment)
    CheckBox chkAddComment;

    @Bind(R.id.chk_close_comment)
    CheckBox chkCloseComment;

    @Bind(R.id.input_title)
    EditText input_title;

    @Bind(R.id.spinner_assigned_by)
    MaterialBetterSpinner spinner_assigned_by;

    @Bind(R.id.spinner_group)
    MaterialBetterSpinner spinner_group;

    @Bind(R.id.spinner_category)
    MaterialBetterSpinner spinner_category;

    @Bind(R.id.spinner_sub_category)
    MaterialBetterSpinner spinner_sub_category;

    @Bind(R.id.spinner_status)
    MaterialBetterSpinner spinner_status;

    @Bind(R.id.spinner_priority)
    MaterialBetterSpinner spinner_priority;

    @Bind(R.id.spinner_assigned_to)
    MaterialBetterSpinner spinner_assigned_to;

    @Bind(R.id.input_details)
    EditText input_details;

    @Bind(R.id.input_comment)
    EditText input_comment;

    @Bind(R.id.scrollView)
    ScrollView scrollView;

    @Bind(R.id.btn_cancel)
    Button btn_cancel;

    @Bind(R.id.btn_open_comments)
    Button btn_open_comments;

    @Bind(R.id.btn_update)
    Button btn_update;

    //    String[] SPINNERLIST = {"Android Material Design", "Material Design Spinner", "Spinner Using Material Library", "Material Spinner Example"};
    public static String TAG = "TicketDetailsFragment";

    HashMap groupDataMap = new HashMap();
    HashMap subCategoryDataMap = new HashMap();
    HashMap adminDataMap = new HashMap();
    HashMap employeeDataMap = new HashMap();
    HashMap priorityDataMap = new HashMap();
    HashMap statusDataMap = new HashMap();
    HashMap categoryDataMap = new HashMap();
    ArrayList<String> mylist = new ArrayList<>();
    View layout;
    TinyDB tinyDB;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        allApiFacade = (IAllApiFacade) IOCContainer.getInstance().getObject(IOCContainer.ObjectName.ALL_API_SERVICE, TAG);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        callbackInterface = (CallbackInterface) getActivity();
        tinyDB = new TinyDB(getActivity());
        context = getActivity();

        layout = inflater.inflate(R.layout.fragment_ticket_details, container,
                false);
        ButterKnife.bind(this, layout);

        //intializeComponent();
        toolbarAdjustment();
        chkAddComment.setOnClickListener(this);
        chkCloseComment.setOnClickListener(this);


        hideKeyboardfromFragment();

        getGroupList();
        getCategoryList();
        getEmployeeList();
        getAdminList();
        getStatusList();
        getPriorityList();

        Thread thread = new Thread(){
            @Override
            public void run() {
                try {
                    progressBarVisibility(true);
                    Thread.sleep(1000);
                    getTicketDetails();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();

        input_details.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.input_details) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        input_comment.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.input_comment) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });


        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }

        });

        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TicketDetailsResponse detailsResponse = (TicketDetailsResponse) tinyDB.getObject("TicketDetailsResponse", TicketDetailsResponse.class);

                if (!(detailsResponse.getData().size() > 0)) {
                    return;
                }

                if (input_title.getText() == null) {
                    Toast.makeText(getActivity(), "Enter Ticket Title", Toast.LENGTH_SHORT).show();
                    return;

                } else if (employeeDataMap == null ||
                        (spinner_assigned_by.getText()!= null) && spinner_assigned_by.getText().toString().equalsIgnoreCase("")||
                        String.valueOf(employeeDataMap.get(String.valueOf(spinner_assigned_by.getText()))) == null) {
                    Toast.makeText(getActivity(), "Select Assigned By", Toast.LENGTH_SHORT).show();
                    return;

                } else if (groupDataMap == null ||
                        (spinner_group.getText()!= null) && spinner_group.getText().toString().equalsIgnoreCase("")||
                        groupDataMap.get(String.valueOf(spinner_group.getText())).toString() == null) {
                    Toast.makeText(getActivity(), "Select Group", Toast.LENGTH_SHORT).show();
                    return;

                } else if (categoryDataMap == null ||
                        (spinner_category.getText() != null) && spinner_category.getText().toString().equalsIgnoreCase("")||
                        categoryDataMap.get(String.valueOf(spinner_category.getText())).toString() == null) {
                    Toast.makeText(getActivity(), "Select  Category", Toast.LENGTH_SHORT).show();
                    return;

                } else if (subCategoryDataMap == null ||
                        (spinner_sub_category.getText() != null) && spinner_sub_category.getText().toString().equalsIgnoreCase("")
                        ||
                        subCategoryDataMap.get(String.valueOf(spinner_sub_category.getText())).toString() == null) {
                    Toast.makeText(getActivity(), "Select Sub Category", Toast.LENGTH_SHORT).show();
                    return;

                } else if (statusDataMap == null ||
                        (spinner_status.getText() != null) && spinner_status.getText().toString().equalsIgnoreCase("")||
                        statusDataMap.get(String.valueOf(spinner_status.getText())).toString() == null) {
                    Toast.makeText(getActivity(), "Select Status", Toast.LENGTH_SHORT).show();
                    return;

                } else if (priorityDataMap == null ||
                        (spinner_priority.getText()!= null) &&  spinner_priority.getText().toString().equalsIgnoreCase("")||
                        priorityDataMap.get(String.valueOf(spinner_priority.getText())).toString() == null) {
                    Toast.makeText(getActivity(), "Select Priority", Toast.LENGTH_SHORT).show();
                    return;

                } else if (adminDataMap == null ||
                        (spinner_assigned_to.getText()!= null) &&  spinner_assigned_to.getText().toString().equalsIgnoreCase("")||
                        adminDataMap.get(String.valueOf(spinner_assigned_to.getText())).toString() == null) {
                    Toast.makeText(getActivity(), "Select Assigned To", Toast.LENGTH_SHORT).show();
                    return;

                } else if (input_details.getText() == null) {
                    Toast.makeText(getActivity(), "Enter ticket details", Toast.LENGTH_SHORT).show();
                    return;

                }

                try {
                    Map<String, String> updatedParams = new HashMap<>();
                    Datum datum = detailsResponse.getData().get(0);
                    updatedParams.put("ticket_id", datum.getTicketId());
                    updatedParams.put("title", input_title.getText() + "");
                    updatedParams.put("assignedBy", String.valueOf(employeeDataMap.get(String.valueOf(spinner_assigned_by.getText()))));
                    updatedParams.put("group", groupDataMap.get(String.valueOf(spinner_group.getText())).toString());
                    updatedParams.put("category", categoryDataMap.get(String.valueOf(spinner_category.getText())).toString());
                    updatedParams.put("subCategory", subCategoryDataMap.get(String.valueOf(spinner_sub_category.getText())).toString());
                    updatedParams.put("status", statusDataMap.get(String.valueOf(spinner_status.getText())).toString());
                    updatedParams.put("priority", priorityDataMap.get(String.valueOf(spinner_priority.getText())).toString());
                    updatedParams.put("assignedTo", adminDataMap.get(String.valueOf(spinner_assigned_to.getText())).toString());
                    updatedParams.put("details", input_details.getText() + "");
                    updatedParams.put("comments", input_comment.getText() + "");
                    updatedParams.put("isCloseWithComment", chkAddComment.isChecked() + "");

                    progressBarVisibility(true);
                    allApiFacade.updateTicket(getActivity(), updatedParams, TAG);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        btn_open_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.COMMENTS_LIST_FRAGMENT), true, R.id.fragmentContainer, true);
            }
        });


        spinner_assigned_by.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Your code here

//                Toast.makeText(adapterView.getContext(),
//                        "OnItemSelectedListener : " + adapterView.getItemAtPosition(i).toString(),
//                        Toast.LENGTH_SHORT).show();

                Log.e(TAG, "************");
                Log.e(TAG, adapterView.getSelectedItem().toString());
                Log.e(TAG, adapterView.getItemAtPosition(i).toString());
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        spinner_assigned_by.addTextChangedListener(this);

        spinner_assigned_to.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Log.e(TAG, "  spinner_assigned_to");
                Log.e(TAG, s + "  onTextChanged");
            }
        });

        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.e(TAG, "************");
        Log.e(TAG, spinner_assigned_by.getText() + "");
        Log.e(TAG, "8080 " + spinner_assigned_by.getListSelection() + "");

    }

    void getEmployeeList() {
        allApiFacade.getEmployeeList(context, null, TAG);
    }

    void getAdminList() {
        allApiFacade.getAdminUserList(context, null, TAG);
    }

    void getStatusList() {
        allApiFacade.getStatusList(context, null, TAG);
    }

    void getPriorityList() {
        allApiFacade.getPriorityList(context, null, TAG);
    }

    void getSubCategoryList(String categoryId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("categoryid", categoryId);
        allApiFacade.getSubCategoryList(context, params, TAG);
    }

    void getCategoryList() {
        allApiFacade.getCategoryList(context, null, TAG);
    }

    void getGroupList() {
        allApiFacade.getGroupList(context, null, TAG);
    }

    void getTicketDetails() {
        Map<String, String> ticketParams = new HashMap<String, String>();

        Intent intent = getActivity().getIntent();
        String ticketId = intent.getStringExtra("ticketId");
        TinyDB tinyDB = TinyDB.getInstance(getActivity());
        Data data = (Data) tinyDB.getObject(USER_DATA, Data.class);
        ticketParams.put("userid", data.getUserid().toString());
        ticketParams.put("ticketid", ticketId);

        allApiFacade.getTicketDetails(context, ticketParams, TAG);
    }

    void toolbarAdjustment() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle("Ticket Details");

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeAsUpIndicator(null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    public void onClick(final View view) {
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch (view.getId()) {
            case R.id.chk_add_comment:
                if (checked) {
                    AnimUtils.expand(layoutCommentView);
                    input_comment.requestFocus();
                    focusOnView();

                } else {
                    AnimUtils.collapse(layoutCommentView);
                }
                break;

            case R.id.chk_close_comment:
                if (checked) {

                } else {

                }
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResponse(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

        if (response instanceof TicketDetailsResponse) {
            progressBarVisibility(false);
            TicketDetailsResponse ticketDetailsResponse = (TicketDetailsResponse) response;

            if (ticketDetailsResponse.getStatus().equalsIgnoreCase(SUCCESS)) {
                tinyDB.putObject("TicketDetailsResponse", ticketDetailsResponse);
                setData(ticketDetailsResponse);
            }

        } else if (response instanceof GroupResponse) {
            GroupResponse groupResponse = (GroupResponse) response;

            if (groupResponse.getStatus().equalsIgnoreCase(SUCCESS)) {

                if (groupResponse.getData().size() > 0) {
                    for (Datum datum : groupResponse.getData()) {
                        groupDataMap.put(datum.getText(), datum.getId());
                        mylist.add(datum.getText());
                    }

                    String[] groupArray = mylist.toArray(new String[0]);
                    setSpinnerAdapter(spinner_group, groupArray);
                    mylist.clear();
                }
            }
        } else if (response instanceof CategoryResponse) {
            CategoryResponse categoryResponse = (CategoryResponse) response;

            if (categoryResponse.getStatus().equalsIgnoreCase(SUCCESS)) {

                if (categoryResponse.getData().size() > 0) {
                    for (Datum datum : categoryResponse.getData()) {
                        categoryDataMap.put(datum.getText(), datum.getId());
                        mylist.add(datum.getText());
                    }

                    String[] categoryArray = mylist.toArray(new String[0]);
                    setSpinnerAdapter(spinner_category, categoryArray);
                    mylist.clear();

                    AdapterView.OnItemClickListener listener = new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                           // Toast.makeText(getActivity(), parent.getAdapter().getItem(position).toString() + "", Toast.LENGTH_SHORT).show();
                            getSubCategoryList(String.valueOf(categoryDataMap.get(parent.getAdapter().getItem(position))));
                            spinner_sub_category.setText(null);

                        }
                    };
                    spinner_category.setOnItemClickListener(listener);

                    String[] stateArray = {};
                    final ArrayAdapter<String> arrayAdapterState = new ArrayAdapter<>(getActivity(),
                            R.layout.simple_dropdown_item, stateArray);
                    spinner_sub_category.setAdapter(arrayAdapterState);
                }
            }
        } else if (response instanceof SubCategoryResponse) {
            SubCategoryResponse subCategoryResponse = (SubCategoryResponse) response;

            if (subCategoryResponse.getStatus().equalsIgnoreCase(SUCCESS)) {

                if (subCategoryResponse.getData().size() > 0) {
                    for (Datum datum : subCategoryResponse.getData()) {
                        subCategoryDataMap.put(datum.getText(), datum.getId());
                        mylist.add(datum.getText());
                    }

                    String[] subCategoryArray = mylist.toArray(new String[0]);
                    setSpinnerAdapter(spinner_sub_category, subCategoryArray);
                    mylist.clear();
                }
            }
        } else if (response instanceof StatusResponse) {
            StatusResponse statusResponse = (StatusResponse) response;

            if (statusResponse.getStatus().equalsIgnoreCase(SUCCESS)) {

                if (statusResponse.getData().size() > 0) {
                    for (Datum datum : statusResponse.getData()) {
                        statusDataMap.put(datum.getText(), datum.getId());
                        mylist.add(datum.getText());
                    }

                    String[] subCategoryArray = mylist.toArray(new String[0]);
                    setSpinnerAdapter(spinner_status, subCategoryArray);
                    mylist.clear();
                }
            }
        } else if (response instanceof PriorityResponse) {
            PriorityResponse priorityResponse = (PriorityResponse) response;

            if (priorityResponse.getStatus().equalsIgnoreCase(SUCCESS)) {

                if (priorityResponse.getData().size() > 0) {
                    for (Datum datum : priorityResponse.getData()) {
                        priorityDataMap.put(datum.getText(), datum.getId());
                        mylist.add(datum.getText());
                    }

                    String[] subCategoryArray = mylist.toArray(new String[0]);
                    setSpinnerAdapter(spinner_priority, subCategoryArray);
                    mylist.clear();
                }
            }
        } else if (response instanceof EmployeeResponse) {
            EmployeeResponse employeeResponse = (EmployeeResponse) response;

            if (employeeResponse.getStatus().equalsIgnoreCase(SUCCESS)) {

                if (employeeResponse.getData().size() > 0) {
                    for (Datum datum : employeeResponse.getData()) {
                        employeeDataMap.put(datum.getText(), datum.getId());
                        mylist.add(datum.getText());
                    }

                    String[] subCategoryArray = mylist.toArray(new String[0]);
                    setSpinnerAdapter(spinner_assigned_by, subCategoryArray);

                    mylist.clear();
                }
            }
        } else if (response instanceof AdminUserResponse) {
            AdminUserResponse adminUserResponse = (AdminUserResponse) response;

            if (adminUserResponse.getStatus().equalsIgnoreCase(SUCCESS)) {

                if (adminUserResponse.getData().size() > 0) {
                    for (Datum datum : adminUserResponse.getData()) {
                        adminDataMap.put(datum.getText(), datum.getId());
                        mylist.add(datum.getText());
                    }

                    String[] subCategoryArray = mylist.toArray(new String[0]);
                    setSpinnerAdapter(spinner_assigned_to, subCategoryArray);
                    mylist.clear();
                }
            }
        } else if (response instanceof UpdateTicketResponse) {
            progressBarVisibility(false);
            UpdateTicketResponse updateTicketResponse = (UpdateTicketResponse) response;
            if (updateTicketResponse.getStatus().equalsIgnoreCase(SUCCESS)) {
//                getFragmentManager().popBackStack();
                getActivity().finish();
            }

            Toast.makeText(context, updateTicketResponse.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    void setSpinnerAdapter(MaterialBetterSpinner spinner, String[] dataArray) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                R.layout.simple_dropdown_item, dataArray);
        spinner.setAdapter(arrayAdapter);

    }

    @Override
    public void onErrorResponse(Exception error, String tag) {
        progressBarVisibility(false);
    }

    void setData(TicketDetailsResponse ticketDetailsResponse) {

        if (!(ticketDetailsResponse.getData().size() > 0)) {
            return;
        }

        if (categoryDataMap.get(ticketDetailsResponse.getData().get(0).getCategory()) != null) {
            getSubCategoryList(String.valueOf(categoryDataMap.get(ticketDetailsResponse.getData().get(0).getCategory().toString())));
        }

        try{
        input_title.setText(ticketDetailsResponse.getData().get(0).getTitle());
        spinner_assigned_by.setText(ticketDetailsResponse.getData().get(0).getCreatorEmailId());
        spinner_group.setText(ticketDetailsResponse.getData().get(0).getGroup());
        spinner_category.setText(ticketDetailsResponse.getData().get(0).getCategory());
        spinner_sub_category.setText(ticketDetailsResponse.getData().get(0).getSubCategory());
        spinner_status.setText(ticketDetailsResponse.getData().get(0).getStatus());
        spinner_priority.setText(ticketDetailsResponse.getData().get(0).getPriority());
        spinner_assigned_to.setText(ticketDetailsResponse.getData().get(0).getTechnicianEmailId());
        input_details.setText(ticketDetailsResponse.getData().get(0).getDetails());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private final void focusOnView() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(btn_cancel.getBottom(), 0);
            }
        });
    }

    private final void focusOnView(final ScrollView scroll, final View view) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                int vLeft = view.getLeft();
                int vRight = view.getRight();
                int sWidth = scroll.getWidth();
                scroll.smoothScrollTo(((vLeft + vRight - sWidth) / 2), 0);
            }
        });
    }

    @Override
    public void afterTextChanged(Editable arg0) {
        Log.e(TAG, arg0 + " afterTextChanged");
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count,
                                  int after) {
        Log.e(TAG, s + " beforeTextChanged");
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.e(TAG, s + "  onTextChanged");
    }

}