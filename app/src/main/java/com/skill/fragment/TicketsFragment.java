package com.skill.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skill.DetailsActivity;
import com.skill.R;
import com.skill.adapter.TicketsRecyclerViewAdapter;
import com.skill.api.IAllApiFacade;
import com.skill.api.IOCContainer;
import com.skill.helper.CallbackInterface;
import com.skill.helper.Constants;
import com.skill.helper.EndlessRecyclerOnScrollListener;
import com.skill.helper.TinyDB;
import com.skill.model.Datum;
import com.skill.model.TicktesResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class TicketsFragment extends BaseFragment implements TicketsRecyclerViewAdapter.ItemClickListener {

    public static String TAG = "TicketsFragment";
    private static Context context;
    static IAllApiFacade allApiFacade;
    TicketsRecyclerViewAdapter adapter;
    CallbackInterface callbackInterface;
    private ArrayList<Datum> ticketList;
    LinearLayoutManager linearLayoutManager;
    int pageNumber = 0;

    @Bind(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    TinyDB tinyDB;

    @Bind(R.id.no_item)
    TextView no_item_text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ticketList = new ArrayList<>();
        adapter = new TicketsRecyclerViewAdapter(activityContext, ticketList);
        adapter.setItemClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        callbackInterface = (CallbackInterface) getActivity();
        context = getActivity();
        tinyDB = TinyDB.getInstance(context);
        View layout = inflater.inflate(R.layout.fragment_all_tickets, container,
                false);
        ButterKnife.bind(this, layout);

        intializeComponent();
        toolbarAdjusment();

        if(!tinyDB.getString(Constants.QUERY).equalsIgnoreCase("")){

            filterTickets();
            tinyDB.putString(Constants.QUERY, "");
        }else {
            getAllTickets();
        }


        return layout;
    }

    void toolbarAdjusment() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setTitle(tinyDB.getString(Constants.TICKETS_TITLE));
    }

    void intializeComponent() {
        ticketList.clear();
        linearLayoutManager = new LinearLayoutManager(activityContext);
        myRecyclerView.setLayoutManager(linearLayoutManager);
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(adapter);

        myRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                // do something...
                pageNumber++;
                getAllTickets();
            }
        });

    }

    void filterTickets() {
//        progressBarVisibility(true);
        allApiFacade = (IAllApiFacade) IOCContainer.getInstance().getObject(IOCContainer.ObjectName.ALL_API_SERVICE, TAG);
        Map<String, String> params = new HashMap<String, String>();
        params.put("page", pageNumber+"");
        params.put("text", tinyDB.getString(Constants.QUERY)+"");

//        allApiFacade.getAllTickets(context, params, TAG);
    }

    void getAllTickets() {

//        progressBarVisibility(true);
        allApiFacade = (IAllApiFacade) IOCContainer.getInstance().getObject(IOCContainer.ObjectName.ALL_API_SERVICE, TAG);
        Map<String, String> params = new HashMap<String, String>();
        params.put("page", pageNumber+"");
        if(!tinyDB.getString(Constants.STATUS_ID).equalsIgnoreCase("")) {
            params.put("status", tinyDB.getString(Constants.STATUS_ID));
        }
//        allApiFacade.getAllTickets(context, params, TAG);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResponse(Object response, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

        if (response instanceof TicktesResponse) {
            progressBarVisibility(false);
            TicktesResponse ticktesResponse = (TicktesResponse) response;
            if (ticktesResponse.getStatus().equalsIgnoreCase(SUCCESS)) {

                if(ticktesResponse.getData().size()>0){
                    ticketList.addAll(ticktesResponse.getData());
                    adapter.notifyDataSetChanged();
                    no_item_text.setVisibility(View.INVISIBLE);
                }else {

                    no_item_text.setVisibility(View.VISIBLE);

                }

            }
        }
    }

    @Override
    public void onErrorResponse(Exception error, String tag) {
        progressBarVisibility(false);
        no_item_text.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onItemClick(int position, View v) {

        if(isAdded()) {
            Datum datum = ticketList.get(position);
            Intent intent = new Intent(context, DetailsActivity.class);
            intent.putExtra("ticketId", datum.getTicketId());
            startActivity(intent);

        }
        //switchFragment(AndroidUtils.getFragment(AndroidUtils.FragmentTag.TICKET_DETAILS_SFRAGMENT), true, R.id.fragmentContainer, false);
    }
}